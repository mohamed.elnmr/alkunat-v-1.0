#!/bin/bash -e 

sudo apt-get -y update 
sudo apt-get -y install php php-fpm php-xml php-mbstring nginx composer git openssh-server # install dependencies

sudo mkdir /app && sudo mkfs -t ext4 /dev/xvdb # making /app and fromat filesystem

sudo mount /dev/xvdb /app && sudo chmod 777 /app # mounting /app 

cd /app
if [ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ]; then ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null; fi # adding ssh
git clone -vv  ssh://git@bitbucket.org/alkunatdevops/sl-phpbackend.git ## cloning project
cd  sl-phpbackend/backend
composer install 
rm -rf ~/.ssh/alkunat ~/.ssh/config  # remove ssh keys
